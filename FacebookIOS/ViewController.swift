
// Swift
//
// Add this to the header of your file, e.g. in ViewController.swift

import FBSDKLoginKit
import FirebaseAuth
import AuthenticationServices

// Add this to the body
class ViewController: UIViewController, LoginButtonDelegate {
    
    let appleProvider = AppleSignInClient()
    let appleBtn = ASAuthorizationAppleIDButton()

    let loginButton = FBLoginButton()
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnFacebook: UIButton!

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var email: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.delegate = self
        loginButton.isHidden = true
        
        txtCode.isHidden = true
        if let token = AccessToken.current,
            !token.isExpired {
            // User is logged in, do work such as go to next view controller.
        }else{
            loginButton.permissions = ["public_profile", "email"]
        }
    }
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let token = AccessToken.current, !token.isExpired{
            let token = token.tokenString
            let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "email, picture.type(large), name, id"], tokenString: token, version: nil, httpMethod: .get)
            request.start(completionHandler: { connection, result, error in

                let object = result as? [String: Any]
                self.email.text = object!["email"]! as? String
                self.userName.text = object!["name"]! as? String
                let pic = object!["picture"] as? [String: Any]
                let data = pic!["data"] as? [String: Any]
                let url = data!["url"]! as! String
               
                let uri = URL(string: url)
                let dataUrl = try? Data(contentsOf: uri!)
                let img = UIImage(data: dataUrl!)
                self.image.image = img
            })
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {

    }

    @IBAction func btnFBClick(_ sender: Any) {
        loginButton.sendActions(for: .touchUpInside)
    }
    var verification_id: String? = nil
    @IBAction func btnPhone(_ sender: Any) {
        if txtCode.isHidden{
            if !txtPhone.text!.isEmpty{
                Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                
                PhoneAuthProvider.provider().verifyPhoneNumber(txtPhone.text!, uiDelegate: nil) { (verificationID, error) in
                  if error != nil {
                    return
                  }else{
                    self.verification_id = verificationID
                    self.txtCode.isHidden = false
                  }
                
                }
            }else{
                print("please inter phone number")
            }
        } else{
            if verification_id != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_id!, verificationCode: txtCode.text!)
                Auth.auth().signIn(with: credential, completion: {autData, error in
                    if error != nil {
                        print(error.debugDescription)
                    }else{
                        print("Auth Sucess" + (autData?.user.phoneNumber)! )
                    }
                })
            }else{
                print("error ")
            }
        }
       
    }
    
    
    @objc func signInWithApple(sender: ASAuthorizationAppleIDButton) {
            appleProvider.handleAppleIdRequest(block: { fullName, email, token in
                
            })
        }
    
    @IBAction func btnAppleLogin(_ sender: Any) {
//        if let authUI =
        appleBtn.addTarget(self, action: #selector(signInWithApple(sender: )), for: .touchUpInside)
                appleBtn.sendActions(for: .touchUpInside)

    }
}
